#!/usr/bin/env bash

TMUXINATOR=~/.config/tmuxinator/
NEOVIM=~/.local/vim_sessions/
VIM=~/.local/vim_sessions/


function get_file {
    cd $SESSION_PATH
    FILE=$(
        fzf \
            --border sharp \
            --margin 15% \
            --padding 10% \
            --info inline \
            --prompt 'SEARCH: ' \
            --pointer '**' \
            --ansi \
            --color 'bg+:-1,pointer:green,fg+:green,hl:yellow,border:gray' \
        | cut -d. -f1
    ) 
}


case $1 in
    --tmux | -t)
        SESSION_PATH=$TMUXINATOR
        get_file

        if [[ -n $FILE ]]; then
            tmuxinator $FILE       
        fi
    ;;


    --nvim | -n)
        SESSION_PATH=$NEOVIM
        get_file

        if [[ -n $FILE ]]; then
            nvim -S $FILE
        fi
    ;;


    --vim | -v)
        SESSION_PATH=$VIM
        get_file

        if [[ -n $FILE ]]; then
            vim -S $FILE
        fi
    ;;


    *)
        ranger $TMUXINATOR $NEOVIM
    ;;
esac
