#!/usr/bin/env bash


string=$(tmux list-windows | grep '*')

slice_1=$(echo $string | awk -F 'layout ' '{ print $2 }')
slice_2=$(echo $slice_1 | awk -F '] @' '{ print $1 }')

layout=$slice_2

echo $layout | xclip -selection primary
