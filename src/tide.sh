#!/usr/bin/env bash


case $1 in

    # Grid style 4x4

    -g)
        tmux split-window -h
        tmux split-window -v

        tmux select-pane -U
        tmux select-pane -L

        tmux split-window -v
        tmux select-pane -U
    ;;


    # IDE style with 2 main panes and 3 at bottom

    **)
        tmux split-window -v -p 30
        tmux split-window -h -p 66
        tmux split-window -h

        tmux select-pane -L
        tmux select-pane -L
        tmux select-pane -U

        tmux split-window -h
        tmux select-pane -L
    ;;
esac
