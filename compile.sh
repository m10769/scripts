#!/usr/bin/env sh

SRC=~/.local/scripts/src
BIN=~/.local/scripts/bin


chmod +x $SRC/*.*


mkdir $BIN
rm $BIN/*
cd $SRC

for i in *.*; do
    FILE_NAME=$(echo $i)
    BIN_NAME=$(echo $i | cut -d. -f1)

    ln -s $SRC/$FILE_NAME $BIN/$BIN_NAME
done
